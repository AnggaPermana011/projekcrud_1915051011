@extends('layout')

@section('konten')
    <h2>Daftar Mahasiswa</h2>
    <div class="float-right">
        <a href="/tambah">
            <button class="btn btn-primary">Add</button>
        </a>
    </div>

    <div>
        <table class="table">
            <tr>
                <th>Nama</th>
                <th>NIM</th>
                <th>Kelas</th>
                <th>Prodi</th>
                <th>Fakultas</th>
            </tr>
            @foreach ($mahasiswa as $m)
            <tr>
                <td>{{ $m->nama_mahasiswa }}</td>
                <td>{{ $m->nim_mahasiswa }}</td>
                <td>{{ $m->kelas_mahasiswa }}</td>
                <td>{{ $m->prodi_mahasiswa }}</td>
                <td>{{ $m->fakultas_mahasiswa }}</td>
                <td>
                    <a href="/edit/{{ $m->id }}"><i class="fa fa-edit"></i> </a>
                    <a href="/delete/{{ $m->id }}" onclick="javascript: return confirm('Yakin ingin hapus data?')"><i class="fa fa-trash"></i> </a>
                </td>
            </tr> 
            @endforeach
        </table>
    </div>
@endsection