<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MahasiswaController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
Route::get('/mahasiswa', [MahasiswaController::class,'mahasiswa']);
Route::get('/tambah', [MahasiswaController::class,'tambahdata']);
Route::post('/mahasiswa/save', [MahasiswaController::class,'savetambahdata']);
Route::get('/edit/{id}', [MahasiswaController::class,'edit']);
Route::post('/mahasiswa/update', [MahasiswaController::class,'update']);
Route::get('/delete/{id}', [MahasiswaController::class,'delete']);