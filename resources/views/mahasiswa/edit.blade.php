@extends('layout')

@section('konten')
<h4>Edit Data</h4>
<hr>
@foreach ($mahasiswa as $m)
<form action="/mahasiswa/update" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="row">
    <div class="col-sm-6">
    <div>
        <input type="hidden" name="id" value="{{ $m->id }}">
    </div>
    <div class="form-group">
        <label for="">Nama Mahasiswa</label>
        <input type="text" name="nama_mahasiswa" required="required" value="{{ $m->nama_mahasiswa }}" class="form-control">
    </div>
    <div class="form-group">
        <label for="">Nim Mahasiswa</label>
        <input type="text" name="nim_mahasiswa" required="required" value="{{ $m->nim_mahasiswa }}" class="form-control">
    </div>
    <div class="form-group">
        <label for="">Kelas Mahasiswa</label>
        <input type="text" name="kelas_mahasiswa" required="required" value="{{ $m->kelas_mahasiswa }}" class="form-control">
    </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label for="">Program Studi Mahasiswa</label>
            <input type="text" name="prodi_mahasiswa" required="required" value="{{ $m->prodi_mahasiswa }}" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Fakultas Mahasiswa</label>
            <input type="text" name="fakultas_mahasiswa" required="required" value="{{ $m->fakultas_mahasiswa }}" class="form-control">
        </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary" onclick="javascript: return confirm('Simpan perubahan data?')">Update</button>
    </div>
    </div>
</div>
</form>
@endforeach
@endsection