<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MahasiswaController extends Controller
{
    public function mahasiswa(){
        $mahasiswa = DB::table('mahasiswa')->get();
        return view('mahasiswa',['mahasiswa'=>$mahasiswa]);
    }

    public function tambahdata(){
        return view('mahasiswa.tambah');
    }

    public function savetambahdata(Request $request){
        DB::table('mahasiswa')->insert([
            'nama_mahasiswa'=>$request->nama_mahasiswa,
            'nim_mahasiswa'=>$request->nim_mahasiswa,
            'kelas_mahasiswa'=>$request->kelas_mahasiswa,
            'prodi_mahasiswa'=>$request->prodi_mahasiswa,
            'fakultas_mahasiswa'=>$request->fakultas_mahasiswa,
        ]);
        return redirect('mahasiswa');
    }

    public function edit($id){
        $mahasiswa = DB::table('mahasiswa')->where('id',$id)->get();

        return view('mahasiswa.edit',['mahasiswa'=>$mahasiswa]);
    }

    public function update(Request $request){
        DB::table('mahasiswa')->where('id',$request->id)->update([
            'nama_mahasiswa'=>$request->nama_mahasiswa,
            'nim_mahasiswa'=>$request->nim_mahasiswa,
            'kelas_mahasiswa'=>$request->kelas_mahasiswa,
            'prodi_mahasiswa'=>$request->prodi_mahasiswa,
            'fakultas_mahasiswa'=>$request->fakultas_mahasiswa,
        ]);
        return redirect('mahasiswa');
    }

    public function delete($id){
        DB::table('mahasiswa')->where('id',$id)->delete();

        return redirect('mahasiswa');
    }

}
