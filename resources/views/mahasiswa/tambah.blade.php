@extends('layout')

@section('konten')
<h4>Tambah Data</h4>
<hr>
<form action="/mahasiswa/save" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="row">
    <div class="col-sm-6">
    <div class="form-group">
        <label for="">Nama Mahasiswa</label>
        <input type="text" name="nama_mahasiswa" class="form-control">
    </div>
    <div class="form-group">
        <label for="">Nim Mahasiswa</label>
        <input type="text" name="nim_mahasiswa" class="form-control">
    </div>
    <div class="form-group">
        <label for="">Kelas Mahasiswa</label>
        <input type="text" name="kelas_mahasiswa" class="form-control">
    </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label for="">Program Studi Mahasiswa</label>
            <input type="text" name="prodi_mahasiswa" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Fakultas Mahasiswa</label>
            <input type="text" name="fakultas_mahasiswa" class="form-control">
        </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary" onclick="javascript: return confirm('Simpan data?')">Simpan</button>
    </div>
    </div>
</div>
</form>
@endsection